(ns core
  (:require [clj-http.client :as client]
            [pantomime.extract :as extract]
            ))

(def education-url "https://susanavet2.skolverket.se/api/1.1/events")

(defn get-education-events []
  (client/get education-url {:accept :json
                             :as :json-strict
                             })
  )

(defn parse-get-education-events-response [response]
  (:content (:body response))
  )


(defn parse-application-urls [e]
  (map :content
       (get-in e [:content :educationEvent :application :url :url]))
  )



(defn parse-education-event [educaton-event]
  (let [application-urls (parse-application-urls educaton-event)]
    application-urls
    )
  )

(def betong-utbildning-url "http://www.folkhogskola.nu/sok-skolor/Blekinge-lan/Blekinge-folkhogskola/Kurser/2021vt/nyborjarkurs-i-betong/")


(defn get-betong-utbildning []
  (let [_ (spit "betong.html" (:body  (client/get betong-utbildning-url)))

        ]
    (extract/parse "betong.html")
    ))

(comment
  (parse-education-event (first (parse-get-education-events-response (get-education-events))))
  ;; found more education data in info subject data https://susanavet2.skolverket.se/api/1.1/infos/i.fbr.288917.361820

  )
